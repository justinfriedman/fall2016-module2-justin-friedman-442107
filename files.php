<?php
session_start();
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Mod2 DropBox Your Files</title>
  <link rel="stylesheet" type="text/css" href="style.css" />
  <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
</head>
<body id="file-bod">

    <div id="files-title"><h1 id="title">Your Files</h1></div>
    <div id="prefs">
      <h2>Preferences</h2>
      <button type="button" onclick="dayMode()">Day Mode (Default)</button>

      <button type="button" onclick="nightMode()">Night Mode</button>

      <button type="button" onclick="funkyMode()">Funky Mode</button>





    </div>



  <?php
    if ($_SESSION["uid"] == "") {
      header("Location: login.html");
    }
    $uDir = "/home/justin/user_files/".$_SESSION["uid"];

$uFiles = scandir($uDir);
$counter = 0;
foreach ($uFiles as $file) {

    if ($counter > 1) {

      echo $file;
      echo '<form action="showFile.php" method="get">
        <input type="hidden" name="file" value='.$file.'>
        <input type="submit" value="View File">
      </form>';
      echo '<form action="delete.php" method="get">
        <input type="hidden" name="file" value='.$file.'>
        <input type="submit" value="Delete">
      </form>';
    }
      //echo "iteration";

      $counter = $counter + 1;
    };

   ?>
   <div class="form-box">


   <form enctype="multipart/form-data" action="uploader.php" method="POST">
	<p>
		<input type="hidden" name="MAX_FILE_SIZE" value="20000000" />
		<label for="uploadfile_input">Choose a file to upload:</label> <input name="uploadedfile" type="file" id="uploadfile_input" />
	</p>
	<p>
		<input type="submit" value="Upload File" />
	</p>
</form>
   </div>
   <div class="form-box">
     <h3>Share a file:</h3>
     <form action="share.php" method="get">
       <input type="text" name="file" value="file name to share">
       <input type="text" name="usrnm" value="other user">
       <input type="submit" value="share">
     </form>
   </div>
   <form action="logOut.php" method="get">
     <input type="submit" value="Log Out">
   </form>
   <script>
   function nightMode() {
       document.body.style.backgroundColor = "rgb(99, 95, 89)";
       document.body.style.backgroundImage = "none";
   }

   function dayMode() {
     document.body.style.backgroundColor = "rgb(36, 104, 175)";
     document.body.style.backgroundImage = "none";
   }

   function funkyMode() {
     document.body.style.backgroundImage = "url('funky.gif')";
   }
   </script>
</body>
</html>
