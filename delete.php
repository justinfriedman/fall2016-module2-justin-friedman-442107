<?php
session_start();
?>

  <?php

  $filename = $_GET['file'];

  if( !preg_match('/^[\w_\.\-]+$/', $filename) ){
	echo "Invalid filename";
	exit;
}

$username = $_SESSION['uid'];
if( !preg_match('/^[\w_\-]+$/', $username) ){
	echo "Invalid username";
	exit;
}

$full_path = sprintf("/home/justin/user_files/%s/%s", $username, $filename);

unlink($full_path);
header('Location: files.php');
  ?>
