<?php
session_start();


  $filename = $_GET['file'];

  if( !preg_match('/^[\w_\.\-]+$/', $filename) ){
	echo "Invalid filename";
	exit;
}

$username = $_SESSION['uid'];
if( !preg_match('/^[\w_\-]+$/', $username) ){
	echo "Invalid username";
	exit;
}

$full_path = sprintf("/home/justin/user_files/%s/%s", $username, $filename);

$finfo = new finfo(FILEINFO_MIME_TYPE);
$mime = $finfo->file($full_path);

header("Content-Type: ".$mime);
header('Content-Disposition: inline; filename="'.basename($full_path).'"'); //from ta help
ob_clean(); // lines 24 and 26 from http://stackoverflow.com/questions/22806647/why-is-my-image-not-being-displayed-correct-when-loading-it-using-readfile

flush();

readfile($full_path);


  ?>
