<?php
session_start();
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Mod2 DropBox LoginCheck</title>
  <link rel="stylesheet" type="text/css" href="style.css" />
  <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
</head>
<body>
  <div id="check-div">


  <?php
    // $login = $_GET['login'];

    if( strpos(file_get_contents("/home/justin/users.txt"),$_GET['login']) !== false) { // taken from http://stackoverflow.com/questions/9059026/php-check-if-file-contains-a-string
      $_SESSION["uid"] = $_GET['login'];
      echo 'Login successful! <a href="files.php">Click me to continue to your files</a>';
    }
    else {
      echo 'Login has failed <a href="login.html">Click me to return to login</a>';
    }
?>
  </div>
</body>
</html>
